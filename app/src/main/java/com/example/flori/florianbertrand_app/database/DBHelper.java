package com.example.flori.florianbertrand_app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

/**
 * Created by flori on 7/05/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "database_album";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating table
        db.execSQL(ImageTable.SQL_CREATE);
        //db.execSQL(AlbumTable.SQL_CREATE);
        db.execSQL(GalleryTable.SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // on upgrade drop older tables
        db.execSQL(GalleryTable.SQL_DELETE);

        db.execSQL(ImageTable.SQL_DELETE);

        // create new table
        onCreate(db);
    }


}
