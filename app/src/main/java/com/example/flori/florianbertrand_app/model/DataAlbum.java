package com.example.flori.florianbertrand_app.model;


import android.content.ContentValues;

import com.example.flori.florianbertrand_app.database.AlbumTable;
import com.example.flori.florianbertrand_app.database.GalleryTable;

public class DataAlbum {

    private String albumId;
    private String image;
    private String title;
    private String description;
    private double latitude;
    private double longitude;

    public DataAlbum(String albumId, String image, String title, String description, double latitude, double longitude) {
        this.albumId = albumId;
        this.image = image;
        this.title = title;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ContentValues toValues(){
        ContentValues values = new ContentValues(6);

        values.put(GalleryTable.COLUMN_ID, albumId);
        values.put(GalleryTable.COLUMN_IDIMAGE, image);
        values.put(GalleryTable.COLUMN_TITLE, title);
        values.put(GalleryTable.COLUMN_DESCRIPTION, description);
        values.put(GalleryTable.COLUMN_LATITUDE, latitude);
        values.put(GalleryTable.COLUMN_LONGITUDE, longitude);
        return values;
    }

    @Override
    public String toString() {
        return "DataAlbum{" +
                "albumId='" + albumId + '\'' +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
