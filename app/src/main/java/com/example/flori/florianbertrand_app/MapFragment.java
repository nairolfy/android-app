package com.example.flori.florianbertrand_app;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;



public class MapFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Marker mMarcadorActual;
    private double[] latitude;
    private double[] longitude;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        latitude = b.getDoubleArray("latitude");
        longitude = b.getDoubleArray("longitude");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);


        SupportMapFragment mapFragment =
                (SupportMapFragment)
                        getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return rootView;
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try{
            for(int i = 0; i<= latitude.length; i++)
            {
                LatLng latLng = new LatLng(latitude[i], longitude[i]);
                mMap.addMarker(new MarkerOptions().position(latLng));
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println("No images!");
        }

    }
}