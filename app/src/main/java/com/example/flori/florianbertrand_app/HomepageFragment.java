package com.example.flori.florianbertrand_app;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class HomepageFragment extends Fragment {
    private Button albumButton;
    private Button mapButton;
    private Button addButton;
    private Callbacks observer;

    public interface Callbacks
    {
        void onAlbumClicked();
        void onMapClicked();
        void onToevoegenClicked();
    }

    public HomepageFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_homepage, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        registerViews();
        albumButton.setOnClickListener(onAlbumClicked);
        mapButton.setOnClickListener(onMapClicked);
        addButton.setOnClickListener(onToevoegenClicked);

    }

    public View.OnClickListener onAlbumClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.onAlbumClicked();
            }
        }
    };

    public View.OnClickListener onMapClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.onMapClicked();
            }
        }
    };

    public View.OnClickListener onToevoegenClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.onToevoegenClicked();
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof Callbacks)
        {
            observer = (Callbacks) activity;
        }
        else
        {
            throw new RuntimeException(activity.toString() + " must implement Callbacks");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        observer = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Homepage");
    }

    private void registerViews()
    {
        View view = getView();
        if(view != null)
        {
            albumButton = view.findViewById(R.id.albumButton);
            mapButton = view.findViewById(R.id.mapButton);
            addButton = view.findViewById(R.id.addButton);
        }
    }
}
