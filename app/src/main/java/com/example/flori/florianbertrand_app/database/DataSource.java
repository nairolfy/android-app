package com.example.flori.florianbertrand_app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.flori.florianbertrand_app.model.DataAlbum;
import com.example.flori.florianbertrand_app.model.DataImage;

import java.util.ArrayList;
import java.util.List;

public class DataSource {

    private static final String TAG = "tag";
    private Context mContext;
    private SQLiteDatabase mDatabase;
    SQLiteOpenHelper mDbHelper;

    public DataSource(Context context){
        this.mContext = context;
        mDbHelper = new DBHelper(mContext);
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void open()
    {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close()
    {
        mDbHelper.close();
    }

    public DataImage createImage(DataImage dataImage){
        ContentValues values = dataImage.toValues();
        mDatabase.insert(ImageTable.TABLE_IMAGE, null, values);
        return dataImage;
    }

    public DataAlbum createAlbum(DataAlbum album)
    {
        //String string = "')";
        ContentValues values = album.toValues();
        mDatabase.insert(GalleryTable.TABLE_GALLERY, null, values);
        Log.d(TAG, "bla");
        //TODO: problemen hier op lossen, lukt niet
        //String sql = "INSERT INTO galleries (albumID, image, title, description, latitude, longitude) VALUES ('" + album.getAlbumId() +"', '"+ album.getImage() +"', '" + album.getTitle() + "', '"+ album.getDescription() + "', '"+ album.getLatitude() + "', '" + album.getLongitude()+"')";
        //mDatabase.execSQL(sql);
        long i = getDataAlbumCount();
        return album;
    }

    public long getDataImageCount()
    {
        open();
        String countQuery = "SELECT  * FROM " + ImageTable.TABLE_IMAGE;
        Cursor cursor = mDatabase.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public long getDataAlbumCount()
    {
        open();
        String countQuery = "SELECT  * FROM " + GalleryTable.TABLE_GALLERY;
        Cursor cursor = mDatabase.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public ArrayList<DataImage> getImages()
    {
        ArrayList<DataImage> images = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("select * from " + ImageTable.TABLE_IMAGE,null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String id = cursor.getString(cursor.getColumnIndex(ImageTable.COLUMN_ID));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(ImageTable.COLUMN_IMAGE));
                DataImage image = new DataImage(id, BitmapFactory.decodeByteArray(blob, 0, blob.length));
                images.add(image);
                cursor.moveToNext();
            }
        }
        return images;
    }

    public ArrayList<DataAlbum> getAlbums()
    {
        ArrayList<DataAlbum> dataAlbums = new ArrayList<>();
        Cursor cursor = mDatabase.rawQuery("select * from " + GalleryTable.TABLE_GALLERY,null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String id = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_ID));
                String imageId = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_IDIMAGE));
                String title = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_TITLE));
                String description = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_DESCRIPTION));
                double latitude = cursor.getDouble(cursor.getColumnIndex(GalleryTable.COLUMN_LATITUDE));
                double longitude = cursor.getDouble(cursor.getColumnIndex(GalleryTable.COLUMN_LONGITUDE));
                DataAlbum  dataAlbum = new DataAlbum(id, imageId, title, description, latitude, longitude);
                dataAlbums.add(dataAlbum);
                cursor.moveToNext();
            }
        }
        return dataAlbums;
    }

    public DataAlbum getAlbum(DataImage image)
    {
        DataAlbum dataAlbum = null;
        Cursor cursor = mDatabase.rawQuery("select * from " + GalleryTable.TABLE_GALLERY,null);
        if(cursor.moveToFirst())
        {
            String id = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_ID));
            String imageId = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_IDIMAGE));
            String title = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_TITLE));
            String description = cursor.getString(cursor.getColumnIndex(GalleryTable.COLUMN_DESCRIPTION));
            double latitude = cursor.getDouble(cursor.getColumnIndex(GalleryTable.COLUMN_LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndex(GalleryTable.COLUMN_LONGITUDE));
            dataAlbum = new DataAlbum(id, imageId, title, description, latitude, longitude);

        }
        return dataAlbum;
    }

    public void updateAlbum(DataAlbum album)
    {
        ContentValues values = album.toValues();
        mDatabase.update(GalleryTable.TABLE_GALLERY, values, GalleryTable.COLUMN_ID + "= ?" ,new String[]{album.getAlbumId()} );
    }

    public void deleteAlbum(DataAlbum  album)
    {
        mDatabase.delete(GalleryTable.TABLE_GALLERY, GalleryTable.COLUMN_ID + "= ?", new String[]{album.getAlbumId()});
        mDatabase.delete(ImageTable.TABLE_IMAGE, ImageTable.COLUMN_IMAGE + "= ?", new String[]{album.getImage()});
    }
}
