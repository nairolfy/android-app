package com.example.flori.florianbertrand_app;

import com.example.flori.florianbertrand_app.model.DataImage;

/**
 * Created by flori on 4/06/2018.
 */

public interface ImageRecyclerViewClickInterface {
    void clickOnImage(DataImage dataImage);
}
