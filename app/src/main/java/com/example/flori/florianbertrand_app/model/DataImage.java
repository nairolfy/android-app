package com.example.flori.florianbertrand_app.model;


import android.content.ContentValues;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.flori.florianbertrand_app.database.ImageTable;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class DataImage implements Parcelable {
    private String imageId;
    private byte[] image;

    public DataImage(String imageId, Bitmap image)
    {
        this.imageId = imageId;
        this.image = getBitmapAsByteArray(image);
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public ContentValues toValues(){
        ContentValues values = new ContentValues(2);

        values.put(ImageTable.COLUMN_ID, imageId);
        values.put(ImageTable.COLUMN_IMAGE, image);
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageId);
        dest.writeByteArray(image);
    }

    public DataImage(Parcel p){
        imageId = p.readString();
        image = new byte[p.readInt()];
        p.readByteArray(image);
    }

    public static final Parcelable.Creator<DataImage> CREATOR
            = new Parcelable.Creator<DataImage>() {
        public DataImage createFromParcel(Parcel in) {
            return new DataImage(in);
        }

        public DataImage[] newArray(int size) {
            return new DataImage[size];
        }
    };

    @Override
    public String toString() {
        return "DataImage{" +
                "imageId='" + imageId + '\'' +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}
