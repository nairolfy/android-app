package com.example.flori.florianbertrand_app.database;

/**
 * Created by flori on 2/06/2018.
 */

public class AlbumTable {
    public static final String TABLE_ALBUM = "albums";
    public static final String COLUMN_ID = "albumID";
    public static final String COLUMN_IDIMAGE = "image";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";

    public static final String SQL_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_ALBUM + "("+
                    COLUMN_ID + " TEXT PRIMARY KEY, " +
                    COLUMN_IDIMAGE + " TEXT, " +
                    COLUMN_TITLE + " TEXT, "+
                    COLUMN_DESCRIPTION + " TEXT, " +
                    COLUMN_LATITUDE + " REAL, " +
                    COLUMN_LONGITUDE + "REAL, " +
                    " FOREIGN KEY ("+ COLUMN_IDIMAGE +") REFERENCES " + ImageTable.TABLE_IMAGE +"("+ImageTable.COLUMN_ID+"));";

    public static  final String SQL_DELETE =
            "DROP TABLE " + TABLE_ALBUM;
}
