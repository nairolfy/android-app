package com.example.flori.florianbertrand_app;


import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.flori.florianbertrand_app.database.DataSource;
import com.example.flori.florianbertrand_app.model.DataImage;

import java.util.ArrayList;


public class AlbumFragment extends Fragment implements ImageRecyclerViewClickInterface  {

    private OnFragmentMainInteractionListener mListener;
    private ArrayList<DataImage> images;
    private RecyclerView recyclerView;

    public AlbumFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        images = b.getParcelableArrayList("images");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_album, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        MyAdapter myAdapter = new MyAdapter(getActivity().getApplicationContext(),images,this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(myAdapter);

        return view;

    }

    @Override
    public void clickOnImage(DataImage dataImage) {

        mListener.navigateToShowDetails(dataImage);
    }



    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentMainInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentMainInteractionListener {
        void navigateToShowDetails(DataImage dataImage);
    }

}
