package com.example.flori.florianbertrand_app.database;

/**
 * Created by flori on 5/06/2018.
 */

public class GalleryTable {
    public static final String TABLE_GALLERY = "galleries";
    public static final String COLUMN_ID = "albumID";
    public static final String COLUMN_IDIMAGE = "image";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";

    public static final String SQL_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_GALLERY + "("+
                    COLUMN_ID + " TEXT PRIMARY KEY, " +
                    COLUMN_IDIMAGE + " TEXT, " +
                    COLUMN_TITLE + " TEXT, "+
                    COLUMN_DESCRIPTION + " TEXT, " +
                    COLUMN_LATITUDE + " REAL, " +
                    COLUMN_LONGITUDE + "REAL, " +
                    "FOREIGN KEY (image) REFERENCES images(imageID));";

    public static  final String SQL_DELETE =
            "DROP TABLE " + TABLE_GALLERY;
}

