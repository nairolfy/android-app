package com.example.flori.florianbertrand_app;

/**
 * Created by flori on 4/06/2018.
 */

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flori.florianbertrand_app.model.DataImage;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {


    private Context context;
    private ArrayList<DataImage> dataImages;
    private LayoutInflater layoutInflater;
    private ImageRecyclerViewClickInterface listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;


        public ViewHolder(View itemView) {
            super(itemView);
            picture = (ImageView) itemView.findViewById(R.id.picture);
        }
    }

    public MyAdapter(Context c, ArrayList<DataImage> dataImages, ImageRecyclerViewClickInterface listener){
        this.context = c;
        this.dataImages = dataImages;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);
        MyAdapter.ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        final DataImage dataImage = dataImages.get(position);
        byte[] i = dataImage.getImage();
        MyAdapter.ViewHolder viewholder =  holder;
        holder.picture.setImageBitmap(BitmapFactory.decodeByteArray(i, 0, i.length));
        holder.picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickOnImage(dataImage);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataImages.size();
    }
}

