package com.example.flori.florianbertrand_app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flori.florianbertrand_app.database.DataSource;
import com.example.flori.florianbertrand_app.model.DataAlbum;
import com.example.flori.florianbertrand_app.model.DataImage;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;

public class PhotoActivity extends AppCompatActivity
        implements PhotoAddFragment.Callbacks, PhotoModFragment.Callbacks{

    private static final int REQ_CODE = 20;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1000;
    private double latitude = 0;
    private double longitude = 0;
    private Bitmap image = null;
    DataSource mDataSource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        int id = (int) getIntent().getExtras().get("name");
        if(id == 1)
        {
            PhotoAddFragment photoAddFragment = new PhotoAddFragment();
            setTitle("Add new photo");

            if(savedInstanceState == null)
            {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, photoAddFragment, "photoAddFragmentTag")
                        .commit();
            }
        }
        if(id == 2)
        {
            PhotoModFragment photoModFragment = new PhotoModFragment();
            setTitle("Modify your photo");
            mDataSource = new DataSource(this);
            mDataSource.open();
            DataImage dataImage = (DataImage) getIntent().getExtras().get("picture");
            String title = (String) getIntent().getExtras().get("title");
            String description = (String) getIntent().getExtras().get("description");
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, photoModFragment, "photoModFragmentTag")
                    .commit();
            fill(dataImage, title, description);
        }
    }

    public void fill(DataImage dataImage, String title, String description)
    {
        TextView t = findViewById(R.id.txtTitle);
        t.setText(title);
        TextView d = findViewById(R.id.txtDescription);
        d.setText(description);
        ImageView i = findViewById(R.id.img);
        i.setImageBitmap(BitmapFactory.decodeByteArray(dataImage.getImage(), 0, dataImage.getImage().length));
    }

    @Override
    public void onLocationClicked() {
        Intent autocompleteActivity = new Intent(getApplicationContext(), AutocompleteActivity.class);
        startActivityForResult(autocompleteActivity, REQ_CODE);
    }

    @Override
    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,
                CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }

    @Override
    public void addPicture() {
        TextView title = findViewById(R.id.titleText);
        TextView description = findViewById(R.id.descriptionText);
        if(title.getText().toString() == "" || description.getText().toString() == "" || image == null || latitude == 0 || longitude == 0)
        {
            return;
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra("image", image);
        returnIntent.putExtra("latitude", latitude);
        returnIntent.putExtra("longitude", longitude);
        returnIntent.putExtra("title",  title.getText().toString());
        returnIntent.putExtra("description", description.getText().toString());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQ_CODE){
            if (resultCode == Activity.RESULT_OK){
                TextView location = findViewById(R.id.txtLocation);
                location.setText((String)data.getExtras().get("name"));
                LatLng latLng = (LatLng) data.getExtras().get("latlng");
                latitude = latLng.latitude;
                longitude = latLng.longitude;
            }
        }
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // convert byte array to Bitmap

                Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                        byteArray.length);
                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
                image = bitmap;

            }
        }
    }

    /*@Override
    protected void onPause() {
        super.onPause();
        mDataSource.close();
    }*/

    /*@Override
    protected void onResume() {
        super.onResume();
        mDataSource.open();
    }*/

    @Override
    public void deletePicture() {
        String id = (String) getIntent().getExtras().get("id");
        DataImage dataImage = (DataImage) getIntent().getExtras().get("picture");
        String imageId = dataImage.getImageId();
        String title = (String) getIntent().getExtras().get("title");
        String description = (String) getIntent().getExtras().get("description");
        double latitude = (double) getIntent().getExtras().get("latitude");
        double longitude = (double) getIntent().getExtras().get("longitude");
        DataAlbum album = new DataAlbum(id, imageId, title, description, latitude, longitude);
        mDataSource.deleteAlbum(album);
    }

    @Override
    public void modifyPicture() {
        String id = (String) getIntent().getExtras().get("id");
        DataImage dataImage = (DataImage) getIntent().getExtras().get("picture");
        String imageId = dataImage.getImageId();
        String title = (String) getIntent().getExtras().get("title");
        String description = (String) getIntent().getExtras().get("description");
        double latitude = (double) getIntent().getExtras().get("latitude");
        double longitude = (double) getIntent().getExtras().get("longitude");
        DataAlbum album = new DataAlbum(id, imageId, title, description, latitude, longitude);
        mDataSource.updateAlbum(album);
    }
}
