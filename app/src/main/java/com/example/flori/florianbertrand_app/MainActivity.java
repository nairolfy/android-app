package com.example.flori.florianbertrand_app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.flori.florianbertrand_app.database.DBHelper;
import com.example.flori.florianbertrand_app.database.DataSource;
import com.example.flori.florianbertrand_app.model.DataAlbum;
import com.example.flori.florianbertrand_app.model.DataImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements HomepageFragment.Callbacks, AlbumFragment.OnFragmentMainInteractionListener{

    private final int REQUEST_CODE_REG = 1;
    private final int REQUEST_CODE_MOD = 2;
    DataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDataSource = new DataSource(this);
        mDataSource.open();

        HomepageFragment homepageFragment = new HomepageFragment();
        setTitle("Homepage");

        if(savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, homepageFragment, "homepageFragmentTag")
                    .commit();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public void onAlbumClicked() {
        AlbumFragment albumFragment = new AlbumFragment();
        ArrayList<DataImage> images = mDataSource.getImages();
        setTitle("Album");
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("images",  images);
        albumFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, albumFragment, "albumFragmentTag")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onMapClicked() {
        MapFragment mapFragment = new MapFragment();
        setTitle("Map");
        ArrayList<DataAlbum> albums = mDataSource.getAlbums();
        double[] latitude = new double[albums.size()];
        double[] longitude = new double[albums.size()];
        try{
            for(int i = 0; i<= albums.size(); i++)
            {
                latitude[i] = albums.get(i).getLatitude();
                longitude[i] = albums.get(i).getLongitude();
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            Toast.makeText(this, "Could not find any images!",
                    Toast.LENGTH_SHORT).show();
        }
        Bundle bundle = new Bundle();
        bundle.putDoubleArray("latitude", latitude);
        bundle.putDoubleArray("longitude", longitude);
        mapFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, mapFragment, "mapFragmentTag")
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onToevoegenClicked() {
        Intent photoActivity = new Intent(getApplicationContext(), PhotoActivity.class);
        photoActivity.putExtra("name", REQUEST_CODE_REG);
        startActivityForResult(photoActivity, REQUEST_CODE_REG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == REQUEST_CODE_REG){
            if (resultCode == Activity.RESULT_OK){
                DataImage image = new DataImage(String.valueOf(mDataSource.getDataImageCount()+1), (Bitmap) data.getExtras().get("image"));
                double latitude = (double) data.getExtras().get("latitude");
                double longitude = (double) data.getExtras().get("longitude");
                String title = (String) data.getExtras().get("title");
                String description = (String) data.getExtras().get("description");
                DataAlbum album = new DataAlbum(String.valueOf(mDataSource.getDataAlbumCount()+1), image.getImageId(), title, description, latitude, longitude);
                try{
                    mDataSource.createImage(image);
                    mDataSource.createAlbum(album);
                }
                catch (SQLiteException e)
                {
                    System.out.println("already added");
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDataSource.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDataSource.open();
    }

    @Override
    public void navigateToShowDetails(DataImage dataImage) {
        DataAlbum dataAlbum = mDataSource.getAlbum(dataImage);
        long count = mDataSource.getDataAlbumCount();
        Intent intent = new Intent(getApplicationContext(), PhotoActivity.class);
        intent.putExtra("picture",dataImage);
        intent.putExtra("id", dataAlbum.getAlbumId());
        intent.putExtra("title" ,dataAlbum.getTitle());
        intent.putExtra("description", dataAlbum.getDescription());
        intent.putExtra("latitude", dataAlbum.getLatitude());
        intent.putExtra("longitude", dataAlbum.getLongitude());
        intent.putExtra("name", REQUEST_CODE_MOD);
        startActivityForResult(intent, REQUEST_CODE_MOD);
    }
}
