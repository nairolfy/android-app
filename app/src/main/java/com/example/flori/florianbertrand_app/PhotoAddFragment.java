package com.example.flori.florianbertrand_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.io.ByteArrayOutputStream;


public class PhotoAddFragment extends Fragment {
        Button cameraButton;
        Button locationButton;
        Button addButton;
        ImageView imageView;

        private Callbacks observer;

        public interface Callbacks
        {
            void onLocationClicked();
            void takePicture();
            void addPicture();
        }
        public PhotoAddFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
                super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

                final View rootView = inflater.inflate(R.layout.fragment_photo_add,
                        container, false);

                return rootView;

        }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        registerViews();
        locationButton.setOnClickListener(onLocationClicked);
        cameraButton.setOnClickListener(takePicture);
        addButton.setOnClickListener(addPicture);

    }

    public View.OnClickListener onLocationClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.onLocationClicked();
            }
        }
    };

    public View.OnClickListener takePicture = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.takePicture();
            }
        }
    };

    public View.OnClickListener addPicture = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(observer != null)
            {
                observer.addPicture();
            }
        }
    };

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof Callbacks)
        {
            observer = (Callbacks) activity;
        }
        else
        {
            throw new RuntimeException(activity.toString() + " must implement Callbacks");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        observer = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Photo");
    }

    private void registerViews()
    {
        View view = getView();
        if(view != null)
        {
            locationButton = view.findViewById(R.id.location);
            cameraButton = view.findViewById(R.id.camera);
            addButton = view.findViewById(R.id.buttonAdd);
        }
    }
}
