package com.example.flori.florianbertrand_app.database;


public class ImageTable {

    public static final String TABLE_IMAGE = "images";
    public static final String COLUMN_ID = "imageID";
    public static final String COLUMN_IMAGE = "image";

    public static final String SQL_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_IMAGE + "("+
                    COLUMN_ID + " TEXT PRIMARY KEY," +
                    COLUMN_IMAGE + " BLOB);";

    public static  final String SQL_DELETE =
            "DROP TABLE " + TABLE_IMAGE;
}
